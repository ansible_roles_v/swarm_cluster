import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')
MANAGER_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('docker_swarm_managers')
WORKER_HOSTS = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('docker_swarm_workers')

def test_docker_swarm_enabled(host):
    assert 'Swarm: active' in host.check_output('docker info')

def test_docker_swarm_status(host):
    docker_info = host.check_output('docker info')
    hostname = host.check_output('hostname -s')
    if hostname in MANAGER_HOSTS:
        assert 'Is Manager: true' in docker_info
        assert 'Nodes: ' in docker_info
        assert 'Managers: ' in docker_info
    elif hostname in WORKER_HOSTS:
        assert 'Is Manager: false' in docker_info
