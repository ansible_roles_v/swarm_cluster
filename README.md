Swarm
=========
Initialize Docker Swarm, register managers and workers.

Requirements
------------
Docker => 1.12

Include role
------------
```yaml
- name: docker
  src: https://gitlab.com/ansible_roles_v/swarm_cluster
  version: main
```

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
    - swarm
```

Warning
=========
Molecule tests don't works!
It will be fixed in the future versions.
